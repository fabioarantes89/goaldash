var path = require("path");

module.exports = {
  development: {
    db: {
      database: "mongodb://localhost/goaldash",
      accessPort: 3000
    },
    push: {
      apn: {
        cert: path.resolve('cert.pem'),
        key: path.resolve('cert.key'),
      },
      gcm: {
        apiKey: ''
      }
    }

  },
  production: {
    db: {
      database: "mongodb://localhost/goaldash",
      accessPort: 80,
    },
    push: {
      apn: {
        cert: path.resolve('cert.pem'),
        key: path.resolve('cert.key'),
      },
      gcm: {
        apiKey: ''
      }
    }
  }
}
