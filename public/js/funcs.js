$(function() {


    $( "#category_autocomplete" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/adm/categories.json",
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            console.log(data.data);
            var arr = [];
            for(var i in data.data){
              arr.push({id: data.data[i]['_id'], label: data.data[i]['name']})
            }

            response(arr);
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
        $()
      }
    });
  });
