var mongoose = require("mongoose");
var fs = require("fs");

function loadSchemas(connection){
  var schemas = {};
  fs.readdir(__dirname+"/models", function (err, files){
    if(err){ return schemas; }
    for(var i in files){
      var file = files[i];
      if(file.substr(0, 1) != "." && file.substr(0, 1) != "_") {
        var schemaName = /([a-z0-9_-]+)\.model\.js/i.exec(file)[1];
        schemas[schemaName] = require(__dirname + "/models/"+file)(connection);
      }
    }
  })
  return schemas;
}
module.exports = function(configs){
  mongoose.connect(configs.database);

  loadSchemas(mongoose);

  return mongoose;;
}
