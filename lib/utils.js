var path = require("path");
var fs = require("fs");
var Promise = require("bluebird");
module.exports = {
  uniqueFilename: function (filename, uniqueID){
    var extension = path.extname(filename);
    filename = filename.replace(extension, "-"+uniqueID+extension);
    return filename;
  },
  generatePath:  function (basePath, Path){
    var bPath = basePath;
    return new Promise(function (res, rej){
      return new Promise(function (resolve, reject){
        var folders = Path.split(path.sep);
        return resolve(folders);
      }).each(function (item){
        return new Promise(function(resolve, reject){
          fs.stat(bPath + path.sep + item, function (err, stats){
            bPath += path.sep + item;
            if(err) {
              fs.mkdir(bPath, function(err, data){
                return resolve(item);
              })
            } else {
              return resolve(item);
            }

          })
        })
      }).finally(function (item){
          return res(basePath + Path);
      })
    })
  }
}
