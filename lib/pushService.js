var PushNotification = require('push-notification');
var DeviceType = PushNotification.DeviceType;
var path = require('path');

PushNotification.init({
	//apn: {
	//	cert: path.resolve('configs/cert_development.pem'),
	//	key: path.resolve('configs/key_development.pem')
	//},
	gcm: {
		apiKey: 'AIzaSyB4xrmkNLvx9VS98UQHDOkfUvgwh7OcHVw'
	}
});

var badge = null;
var sound = null;
var payload = null;

function sendSingleMessage(message, deviceID, deviceType){
	PushNotification.pushSingle(deviceType, deviceID, message, badge, sound, payload);
}
function sendMultipleMessages(message, devices){
		PushNotification.prepare(message, badge, sound, payload);
		devices.map(function (device){
				if(!device.deviceID || !device.deviceType || ['apn', 'gcm'].indexOf(device.deviceType) < 0){
					throw new Error("Invalid Device Item");
				} else {
					PushNotification.addTarget("gcm", device.deviceID);
				}

		})
		PushNotification.push();
}
function PushDevice(deviceID, deviceType){
	this.deviceID = deviceID;
	this.deviceType = "gcm"; //deviceType;

	return this;
}
module.exports = function (){
	return {
		'sendSingleMessage': sendSingleMessage,
		'PushDevice': PushDevice,
		'sendMultipleMessages': sendMultipleMessages
	}
}
