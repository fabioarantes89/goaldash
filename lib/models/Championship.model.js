module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: String,
    type: String,
    date_start: Date,
    date_end: Date,
    owner_id: {type: Schema.Types.ObjectId, ref: "User"},
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
    matches: [{type: Schema.Types.ObjectId, ref: "Match"}]
  });
  return mongoose.model('Championship', schema);
}
