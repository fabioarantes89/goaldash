module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: String,
    address: String,
    zipcode: String,
    loc: {
      type: [Number],
      index: '2d',
      required: true
    },
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
  });
  return mongoose.model('Place', schema);
}
