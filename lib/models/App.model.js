var bcrypt = require("bcrypt"),
    SALT_WORK_FACTOR = 10;
module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var appSchema = new Schema({
    name: {type: String, required: true},
    secret: {type: String},
    status: {type: Number}
  });
  appSchema.pre('save', function(next){
    var app = this;
    // Only change secret if name's changed
    if(!app.isModified("name")) return next();


    // Generate Salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
      if(err) return next(err);
      var date = new Date();

      // Generate Hash
      bcrypt.hash(app.name+"-imaginateStudios"+date, salt, function (err, hash){
        if(err) return next(err);

        app.secret = hash;
        next();

      })

    })


  })
  return mongoose.model('App', appSchema);
}
