var bcrypt = require("bcrypt"), SALT_WORK_FACTOR = 10, moment = require("moment");
module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    token: String,
    valid_until: {type: Date},
    app_id: {type: Schema.Types.ObjectId, ref: "App"},
    user_id: {type: Schema.Types.ObjectId, ref: "User"},
  });
  schema.set('toJSON', {transform: function (doc, ret, options) {
    // remove the _id of every document before returning the result
    delete ret._id;
    delete ret.app_id;
    delete ret.__v;
    //delete ret.user_id;

  }});

  schema.pre('save', function(next){
    var user = this;
    // Generate Salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
      if(err) return next(err);

      user.valid_until = moment().add(1, 'years').toDate();

      var date = new Date();
      // Generate Hash
      bcrypt.hash("imaginate"+date+user.app_id+user.user_id, salt, function (err, hash){
        if(err) return next(err);
        user.token = hash;
        next();
      })
    })
  })
  return mongoose.model('Token', schema);
}
