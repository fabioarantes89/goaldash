module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: String,
    match_in: Date,
    score_home: {type: Number, default: 0},
    score_visitor: {type: Number, default: 0},
    team_home: {type: Schema.Types.ObjectId, ref: "Team"},
    team_visitor: {type: Schema.Types.ObjectId, ref: "Team"},
    place: {type: Schema.Types.ObjectId, ref: "Place"},
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
    accepts_home: { type: String, default: "approving", enum: ["approving", "accepted", "refused"]},
    accepts_visitor: { type: String, default: "approving", enum: ["approving", "accepted", "refused"]}
  });
  return mongoose.model('Match', schema);
}
