module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: String,
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
  });
  return mongoose.model('Modality', schema);
}
