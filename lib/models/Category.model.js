module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: {type: String, required: true},
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
  });
  return mongoose.model('Category', schema);
}
