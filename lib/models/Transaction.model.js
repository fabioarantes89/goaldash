var moment = require("moment");
module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    user_id: {type: Schema.Types.ObjectId, ref: "User", required: true},
    payment_id: {type: String},
    date: {type: Date, default: Date.now},
    valid_until: {type: Date, default: moment().add(1, "M").toDate()},
    credits: {type: Number, required: true}
  });
  schema.set('toJSON', {transform: function (doc, ret, options) {
    // remove the _id of every document before returning the result
    delete ret.__v;
  }});

  return mongoose.model('Transaction', schema);
}
