module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: {type:String, required: true},
    description: {type: String},
    uniform: {type: String},
    uniform2: {type: String},
    region: {type: String, required: true},
    city: {type: String, required: true},
    uf: {type: String, required: true},
    type: {type: String,
      required: true,
      default: "TEAM_VISITOR",
      enum: ["TEAM_PRINCIPAL", "TEAM_VISITOR"]
    },
    loc: {
      type: [Number],
      index: '2d',
      required: true
    },
    owner: {type: Schema.Types.ObjectId, ref: "User", required: true},
    file: {type: Schema.Types.ObjectId, ref: "File"},
    category: {type: Schema.Types.ObjectId, ref: "Category"},
    modality: {type: Schema.Types.ObjectId, ref: "Modality"},
    players: [{type: Schema.Types.ObjectId, ref: "User"}]
  });
  return mongoose.model('Team', schema);
}
