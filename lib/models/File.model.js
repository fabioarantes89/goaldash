var moment = require("moment");

module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    filename: String,
    type: String
  });
  schema.set('toJSON', {transform: function (doc, ret, options) {
    var d = moment(doc._id.getTimestamp());
    ret.created_at = d.toISOString();
    ret.path = "/uploads/" + d.format("YYYY/MM/DD") + "/"+ret.filename;
    delete ret.__v;

  }});
  return mongoose.model('File', schema);
}
