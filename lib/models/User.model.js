var bcrypt = require("bcrypt"), SALT_WORK_FACTOR = 10, uniqueValidator = require("mongoose-unique-validator");
var PushNotification = require('push-notification');
var DeviceType = PushNotification.DeviceType;
var Promise = require("bluebird");

module.exports = function (mongoose){
  var Schema = mongoose.Schema;
  var schema = new Schema({
    name: {type: String, required: [true, 'Name Required']},
    rg: {type: String, required: [true, 'RG Required']},
    cpf: {type: String, required: [true, 'CPF Required'], unique: true},
    telephone: {type: String, required: [true, 'Telephone Required']},
    email: {
      type: String,
      required: true,
      unique: true
    },
    salt: {type: String, required: true},
    type: {
      type: Number,
      required: true,
      default: 0,
      enum: [1, 0]
    },
    device_id: {
      type: String,
      required: false
    },
    device_type: {
      type: String,
      required: false,
      enum: [DeviceType.IOS, DeviceType.ANDROID]
    },
    plays_at: {
      type: String,
      required: false,
      enum: ['Goleiro', 'Meia', 'Atacante', 'Zagueiro', 'Volante', 'Lateral', 'Centro-avante']
    },
    file_id: {type: Schema.Types.ObjectId, ref: "File"},
  });
  schema.plugin(uniqueValidator);
  schema.set('toJSON', {transform: function (doc, ret, options) {
    // remove the _id of every document before returning the result
    delete ret.__v;
    delete ret.salt;
    delete ret.device_type;
    delete ret.device_id;

  }});
  schema.methods.comparePass = function (password){
    var user = this;
    return new Promise(function(resolve, reject){

      bcrypt.compare(password, user.salt, function (err, res){
        if( err || !res) { return reject(new Error("Password mismatch")); }
        return resolve(res);
      })
    })
  }

  schema.pre('save', function(next){
    var user = this;
    // Only change secret if name's changed
    if(!user.isModified("salt")) return next();


    // Generate Salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
      if(err) return next(err);

      // Generate Hash
      bcrypt.hash(user.salt, salt, function (err, hash){
        if(err) return next(err);
        user.salt = hash;
        next();
      })
    })
  })
  return mongoose.model('User', schema);
}
