// Set mode
var APP_MODE = (process.env.NODE_ENV)?process.env.NODE_ENV:"development";
var configs = require(__dirname + "/configs/configs.js")[APP_MODE];
var utils = require(__dirname + "/lib/utils.js");
var Promise = require("bluebird");
var database = require(__dirname + "/lib/database.js")(configs.db);
var moment = require("moment");

var PushService = require(__dirname+"/lib/pushService.js")();
function pushServ(data){
  return new Promise(function (resolve, reject){

    var pushList = [];
    data.map(function (team){
        if(team.owner.device_id && team.owner.device_type){
          pushList.push(new PushService.PushDevice(team.owner.device_id, team.owner.device_type));
        }
        team.players.map(function (player){
          if(player.device_id && player.device_type){
            pushList.push(new PushService.PushDevice(player.device_id, player.device_type));
          }
        })
    })
    if(pushList.length > 0){
      PushService.sendMultipleMessages("TEAM_MATCH_WILL_START_SOON", pushList);
    }
    return resolve();
  })
}


function getNextMatches(){
  var messages = [];
  return new Promise(function (resolve, reject){
    console.log("SEARCHING MATCHES");
    database.models.Match.find({accepts_home: {$eq: "accepted"}, accepts_visitor: {$eq: "accepted"}, match_in: {$gt: moment().toDate(), $lt: moment().add(30, "minutes").toDate()}}).populate({path: "team_home", populate: {path: "owner players"}}).populate({path: "team_visitor", populate: {path: "owner players"}}).exec().then(function (data){
      data.map(function (match){
        messages.push(match.team_home);
        messages.push(match.team_visitor);
      })
      pushServ(messages);
    })
  })
}
setTimeout(function (){
  setInterval(getNextMatches, 30000);
}, 500);
