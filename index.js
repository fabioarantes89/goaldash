// Set mode
var APP_MODE = (process.env.NODE_ENV)?process.env.NODE_ENV:"development";

// Init Dependencies
var express = require("express");
var configs = require(__dirname + "/configs/configs.js")[APP_MODE];
var bodyParser = require("body-parser");
var multer = require("multer");
var utils = require(__dirname + "/lib/utils.js");
var moment = require("moment");
//utils.generatePath(__dirname + "/uploads", "/2016/03/01");

var storage = multer.diskStorage({
  destination: function(req, file, cb){
    utils.generatePath(__dirname + "/uploads", moment().format("/YYYY/MM/DD")).then(function (path){
      cb(null, path);
    })

  },
  filename: function (req, file, cb){
    var fName = utils.uniqueFilename(file.originalname, moment().unix());
    cb(null, fName);
  }
});
var upload = multer({storage: storage});
var database = require(__dirname + "/lib/database.js")(configs.db);

// Setting up app
var app = express();
app.locals.uploads = upload;
app.locals.database = database;



// Add Body Parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

function loadApp(name){
  var instance = require(__dirname+"/apps/"+name+".js")(app.locals);
  return instance;
}

var adm = loadApp("adm");
var api = loadApp("api");

//app.use("/adm", adm);
app.use("/", api);

app.listen(3000, function (){
  console.log("App up and running");
})
