module.exports = function (app){
  app.get("/places/new", function (req, res){
    return res.render("places_form", {formTarget: "/adm/places/new/", titleForm: "Create Place", "data": {}});
  })
  app.post("/places/new", function (req, res){
      var user = app.locals.database.models.Place.create(req.body).then(function (){
        return res.redirect("/adm/places/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          return res.render("places_form", {formTarget: "/adm/places/new/", titleForm: "Create Place", "data": req.body});
      });
  })
  app.get("/places/:id", function (req, res, next){
    app.locals.database.models.Place.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      return res.render("places_form", {formTarget: "/adm/places/"+req.params.id, titleForm: "Edit Place", "data": data[0]});
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/places/:id/delete", function (req, res, next){
    app.locals.database.models.Place.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/places/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/places/:id", function (req, res, next){
    var Users =  app.locals.database.models.Place;
    Users.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;
      return res.render("places_form", {formTarget: "/adm/places/"+req.params.id, titleForm: "Edit Place", "data": data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/places", function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    app.locals.database.models.Place.count().exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Place.find().skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.render("places_index");
      })
    })
    .catch(function(err){
      return next(err);
    })


  })

}
