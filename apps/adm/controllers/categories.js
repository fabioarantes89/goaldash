module.exports = function (app){
  app.get("/categories/new", function (req, res){
    return res.render("categories_form", {formTarget: "/adm/categories/new/", titleForm: "Create Category", "data": {}});
  })
  app.post("/categories/new", function (req, res){
      var user = app.locals.database.models.Category.create(req.body).then(function (){
        return res.redirect("/adm/categories/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          return res.render("categories_form", {formTarget: "/adm/categories/new/", titleForm: "Create Category", "data": req.body});
      });
  })
  app.get("/categories/:id", function (req, res, next){
    app.locals.database.models.Category.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      return res.render("categories_form", {formTarget: "/adm/categories/"+req.params.id, titleForm: "Edit Category", "data": data[0]});
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/categories/:id/delete", function (req, res, next){
    app.locals.database.models.Category.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/categories/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/categories/:id", function (req, res, next){
    var Users =  app.locals.database.models.Category;
    Users.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;
      return res.render("categories_form", {formTarget: "/adm/categories/"+req.params.id, titleForm: "Edit Category", "data": data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/categories(\.json)?", function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }


    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;
    var query = {};

    if(req.query.term){
      query.name = new RegExp(req.query.term, "i");
    }

    app.locals.database.models.Category.count(query).exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Category.find(query).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        if(req.params[0] == ".json"){
            return res.json(res.locals);
        }
        return res.render("categories_index");
      })
    })
    .catch(function(err){
      return next(err);
    })


  })

}
