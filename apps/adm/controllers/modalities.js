module.exports = function (app){
  app.get("/modalities/new", function (req, res){
    return res.render("modalities_form", {formTarget: "/adm/modalities/new/", titleForm: "Create Modality", "data": {}});
  })
  app.post("/modalities/new", function (req, res){
      var user = app.locals.database.models.Modality.create(req.body).then(function (){
        return res.redirect("/adm/modalities/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          return res.render("modalities_form", {formTarget: "/adm/modalities/new/", titleForm: "Create Modality", "data": req.body});
      });
  })
  app.get("/modalities/:id", function (req, res, next){
    app.locals.database.models.Modality.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      return res.render("modalities_form", {formTarget: "/adm/modalities/"+req.params.id, titleForm: "Edit Modality", "data": data[0]});
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/modalities/:id/delete", function (req, res, next){
    app.locals.database.models.Modality.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/modalities/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/modalities/:id", function (req, res, next){
    var Users =  app.locals.database.models.Modality;
    Users.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;
      return res.render("modalities_form", {formTarget: "/adm/modalities/"+req.params.id, titleForm: "Edit Modality", "data": data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/modalities", function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    app.locals.database.models.Modality.count().exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Modality.find().skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.render("modalities_index");
      })
    })
    .catch(function(err){
      return next(err);
    })


  })

}
