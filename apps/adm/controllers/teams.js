module.exports = function (app){
  var controllerRoute = "teams";
  var controllerTemplatePrefix = "teams_";
  var formTitles = {new: "Create Team", edit: "Edit Team"}

  app.get("/"+controllerRoute+"/new", function (req, res){
    return res.render(controllerTemplatePrefix+"form", {formTarget: "/adm/"+controllerRoute+"/new/", titleForm: formTitles.new, user: {type: 0}});
  })
  app.post("/"+controllerRoute+"/new", function (req, res){
      app.locals.database.models.Team.create(req.body).then(function (){
        return res.redirect("/adm/"+controllerRoute+"/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          return res.render(controllerTemplatePrefix+"form", {formTarget: "/adm/"+controllerRoute+"/new/", titleForm: formTitles.new, user: req.body});
      });
  })
  app.param("team_id", function (req, res, next){
    app.locals.database.models.Team.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      res.locals.user = data[0];
      return next();
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/"+controllerRoute+"/:team_id", function (req, res, next){
    return res.render(controllerTemplatePrefix+"form", {formTarget: "/adm/"+controllerRoute+"/"+req.params.id, titleForm: formTitles.edit});
  })
  app.get("/"+controllerRoute+"/:team_id/delete", function (req, res, next){
    app.locals.database.models.Team.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/"+controllerRoute+"/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/"+controllerRoute+"/:team_id", function (req, res, next){
    var Team =  app.locals.database.models.Team;
    Team.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;
      return res.render(controllerTemplatePrefix+"form", {formTarget: "/adm/"+controllerRoute+"/"+req.params.id, titleForm: formTitles.edit, user: data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/"+controllerRoute, function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    var Model =  app.locals.database.models.Team;

    Model.count().exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return Model.find().skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.render(controllerTemplatePrefix+"index");
      })
    })
    .catch(function(err){
      return next(err);
    })
  })

}
