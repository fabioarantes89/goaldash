module.exports = function (app){
  app.get("/users/new", function (req, res){
    return res.render("users_form", {formTarget: "/adm/users/new/", titleForm: "Create user", user: {type: 0}});
  })
  app.post("/users/new", function (req, res){
      var user = app.locals.database.models.User.create(req.body).then(function (){
        return res.redirect("/adm/users/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          console.log(res.locals.errors);
          return res.render("users_form", {formTarget: "/adm/users/new/", titleForm: "Create user", user: req.body});
      });
  })
  app.get("/users/:id", function (req, res, next){
    app.locals.database.models.User.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      return res.render("users_form", {formTarget: "/adm/users/"+req.params.id, titleForm: "Create user", user: data[0]});
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/users/:id/delete", function (req, res, next){
    app.locals.database.models.User.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/users/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/users/:id", function (req, res, next){
    var Users =  app.locals.database.models.User;
    Users.findOneAndUpdate({_id: req.params.id}, req.body.data, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;
      return res.render("users_form", {formTarget: "/adm/users/"+req.params.id, titleForm: "Create user", user: data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/users", function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    app.locals.database.models.User.count().exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.User.find().skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.render("users_index");
      })
    })
    .catch(function(err){
      return next(err);
    })


  })

}
