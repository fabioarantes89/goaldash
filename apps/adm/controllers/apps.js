module.exports = function (app){
  app.get("/apps/new", function (req, res){
    return res.render("apps_form", {formTarget: "/adm/apps/new/", titleForm: "Create App", "data": {}});
  })
  app.post("/apps/new", function (req, res){
      var user = app.locals.database.models.App.create(req.body).then(function (){
        return res.redirect("/adm/apps/?success=true")
      }).catch(function (err){
          res.locals.errors = err.errors;
          return res.render("apps_form", {formTarget: "/adm/apps/new/", titleForm: "Create App", "data": req.body});
      });
  })
  app.get("/apps/:id", function (req, res, next){
    app.locals.database.models.App.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]){ return next(new Error("Page not Found"));}
      return res.render("apps_form", {formTarget: "/adm/apps/"+req.params.id, titleForm: "Edit App", "data": data[0]});
    }).catch(function (err){
      return next(err);
    })
  })
  app.get("/apps/:id/delete", function (req, res, next){
    app.locals.database.models.App.remove({_id: req.params.id}).exec().then(function (data){
      return res.redirect("/adm/apps/?deleted=true")
    }).catch(function (err){
      return next(err);
    })
  })
  app.post("/apps/:id", function (req, res, next){
    var Users =  app.locals.database.models.App;

    Users.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}).exec().then(function (data){
      res.locals.savedSuccess = true;

      return res.render("apps_form", {formTarget: "/adm/apps/"+req.params.id, titleForm: "Edit App", "data": data});
    }).catch(function (err){
      return next(err);
    })
  })


  app.all("/apps", function (req, res, next){
    if(req.query.success){
      res.locals.savedSuccess = true;
    }
    if(req.query.deleted){
      res.locals.deletedSuccess = true;
    }
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    app.locals.database.models.App.count().exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.App.find().skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.render("apps_index");
      })
    })
    .catch(function(err){
      return next(err);
    })


  })

}
