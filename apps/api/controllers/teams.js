var express = require("express");
var router = express.Router();
var mongoose = require('mongoose');
var ObjectID = mongoose.Types.ObjectId;
var _ = require("underscore");

module.exports = function (app){


  function isOwner(req, res, next){
    console.log(res.locals)
    if(!res.locals.data.owner && !res.locals.data.owner._id){
      return next(new Error("Team Disabled"));
    }
    if(String(res.locals.user_id) != String(res.locals.data.owner._id)){
        return next(new Error("You don't have permission to do that"));
    } else {
      return next();
    }
  }
  // Param
  router.param("id", function (req, res, next){
    app.locals.database.models.Team.find({_id: req.params.id}).populate("owner category modality file").exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  router.param("match_id", function (req, res, next){
    console.log(req.params);

    app.locals.database.models.Match.find({_id: req.params.match_id}).exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.match = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  // SHOW
  router.get("/:id", function(req, res){
    return res.json(res.locals.data);
  })
  // Players
  router.get("/:id/players", function(req, res){
    app.locals.database.models.User.find({_id: {$in: res.locals.data.players}}).exec().then(function (data){
        return res.json(data);
    })
  })
  router.get("/:id/matches/:match_id/accept", app.locals.hasToken, isOwner, function (req, res, next){
    var values = {};
    //
    console.log(res.locals.data);
    console.log(res.locals);

    if(String(res.locals.match.team_visitor) == String(res.locals.data._id)){
      values.accepts_visitor = "accepted";
    } else if(String(res.locals.match.team_home) == String(res.locals.data._id))
    {
      values.accepts_home = "accepted";
    } else {
      return next(new Error("Invalid Match ID"));
    }
    app.locals.database.models.Match.findOneAndUpdate({_id: res.locals.match._id}, values, {new: true, runValidators: true}).exec().then(function (data){
        return res.json(data);
    }).catch(function (err){
        return next(err);
    })
  })
  router.get("/:id/matches/:match_id/refuse", app.locals.hasToken, isOwner, function (req, res, next){
    var values = {};

    if(String(res.locals.match.team_visitor) == String(res.locals.data._id)){
      values.accepts_visitor = "refused";
    } else if(String(res.locals.match.team_home) == String(res.locals.data._id))
    {
      values.accepts_home = "refused";
    } else {
      return next(new Error("Invalid Match ID"));
    }
    app.locals.database.models.Match.findOneAndUpdate({_id: res.locals.match._id}, values, {new: true, runValidators: true}).exec().then(function (data){
        return res.json(data);
    }).catch(function (err){
        return next(err);
    })
  })
  router.get("/:id/matches", app.locals.hasToken, function (req, res, next){
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):5;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;
    var query = {$or: [{team_home: res.locals.data._id}, {team_visitor: res.locals.data._id}]};

    return app.locals.database.models.Match.count(query).exec().then(function (count){

      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Match.find(query).populate({path: "team_home", populate: {path: "file"}}).populate({path: "team_visitor", populate: {path: "file"}}).populate({path: "place"}).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        delete res.locals.app_id;
        delete res.locals.user_id;

        return res.json(res.locals);
      })
    }).catch(function (data){
      return next(data);
    })
  })


  router.put("/:id/players", app.locals.hasToken, isOwner, function(req, res){
    //res.locals.data.players

    if( typeof(req.body.players) == "string"){
      req.body.players = [req.body.players];
    }
    app.locals.database.models.User.find({_id: {$in: req.body.players}}).exec().then(function (data){
        res.locals.data.players = data;
        return res.locals.data.save().then(function (){
          return res.status(202).end();
        })
    }).catch(function (err){
      return res.status(500).json(err);
    })


  })
  // UPDATE
  router.put("/:id", app.locals.hasToken, isOwner, function(req, res){
    app.locals.database.models.Team.findOneAndUpdate({_id: req.params.id}, req.body, {new: false, runValidators: true}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // DELETE
  router.delete("/:id", app.locals.hasToken, function (req, res){
    app.locals.database.models.Team.remove({_id: req.params.id}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // CREATE
  router.post("/", app.locals.hasToken, function (req, res){
    app.locals.database.models.Team.create(req.body).then(function (data){
      return res.status(202).json(data);
    }).catch(function (err){
        return res.json(err.errors).status(500);
    });
  })
  // LIST
  router.get("/", function(req, res, next){
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;

    var query = {};

    var maxDistance = (!req.query.distance)?8:parseInt(req.query.distance);
    maxDistance /= 6371;

    if(req.query.latitude && req.query.longitude){
      query.loc = {
        $near: [req.query.longitude, req.query.latitude],
        $maxDistance: maxDistance
      }
    }

    if(req.query.term){
      query.name = new RegExp(req.query.term, "i");
    }
    if(/^(TEAM_VISITOR|TEAM_PRINCIPAL)$/i.exec(req.query.teamType)){
      query.type = {$eq: req.query.teamType.toUpperCase()}
    }
    if(req.query.category){
      query.category = {$eq: req.query.category}
    }

    var populates = "file";
    if(req.query.completeOwner){
      populates = "file owner"
    }


    app.locals.database.models.Team.count(query).exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Team.find(query).populate(populates).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;

        return res.json(res.locals);

      })
    })
    .catch(function(err){
      return next(err);
    })
  })
  app.use("/teams", router);
}
