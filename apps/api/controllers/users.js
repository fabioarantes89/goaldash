var express = require("express");
var router = express.Router();

module.exports = function (app){
  // Param
  router.param("id", function (req, res, next){
    app.locals.database.models.User.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  // ME
  router.get("/me", app.locals.hasToken, function (req, res, next){
    app.locals.database.models.User.find({_id: res.locals.user_id}).exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return res.json(res.locals.data);
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  router.put("/me/device", app.locals.hasToken, function (req, res, next){

    for(var i in req.body){
      if(["device_id", "device_type"].indexOf(i) < 0){
        delete req.body[z];
      }
    }
    if(!req.body.device_id || !req.body.device_type){
      return res.status(503).end();
    }

    app.locals.database.models.User.findOneAndUpdate({_id: res.locals.user_id}, req.body, {new: false, runValidators: true}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (e){
      return res.status(503).json(e).end();
    })
  })

  // Transactions
  router.get("/me/transactions", app.locals.hasToken, function (req, res, next){
    app.locals.database.models.Transaction.find({user_id: res.locals.user_id}).then(function (data){
      return res.json(data);
    }).catch(function (err){
      return next(err);
    })
  })
  router.put("/me/transactions", app.locals.hasToken, function (req, res, next){
    req.body.user_id = req.locals.user_id;
    req.body.credits = Number((String(req.body.credits)).replace(",", "."));
    app.locals.database.models.Transaction.create(req.body).then(function (data){
      return res.status(202).json(data);
    }).catch(function (err){
        return res.json(err.errors).status(500);
    });

  })
  // SHOW
  router.get("/:id", app.locals.hasToken, function(req, res){
    return res.json(res.locals.data);
  })
  // UPDATE
  router.put("/:id", app.locals.hasToken, function(req, res){
    if(req.locals.user_id != req.params.id){
      if(req.body.device_id){
        delete req.body.device_id;
      }
      if(req.body.device_type){
        delete req.body.device_type;
      }
    }
    app.locals.database.models.User.findOneAndUpdate({_id: req.params.id}, req.body, {new: false, runValidators: true}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // DELETE
  /*
  router.delete("/:id", app.locals.hasToken, function (req, res){
    app.locals.database.models.User.remove({_id: req.params.id}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  */
  // CREATE
  router.post("/", function (req, res){
    app.locals.database.models.User.create(req.body).then(function (data){
      return res.status(202).json(data);
    }).catch(function (err){
      return res.json(err.errors).status(500);
    });
  })
  // LIST
  router.get("/", app.locals.hasToken, function(req, res){
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;
    var query = {};

    if(req.query.term){
      query.name = new RegExp(req.query.term, "i");
    }

    app.locals.database.models.User.count(query).exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.User.find(query).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.json(res.locals);

      })
    })
    .catch(function(err){
      return next(err);
    })
  })

  app.use("/users", router);
}
