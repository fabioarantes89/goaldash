var express = require("express");
var router = express.Router();
var bcrypt = require("bcrypt");

module.exports = function (app){
  // SHOW
  router.post("/token", function(req, res, next){

    app.locals.database.models.App.find({secret: req.body.appSecret}).exec().then(function (apps){
        if(!apps[0]) { return next(new Error("App not found"));}
        res.locals.app = apps[0];
        return app.locals.database.models.User.find({email: req.body.email}).exec()
    }).then(function (user){
      if(!user[0]) { return next(new Error("User not found"));}
      res.locals.user = user[0];
      return res.locals.user.comparePass(req.body.pass);
    }).then(function (){
      return app.locals.database.models.Token.create({user_id: res.locals.user._id, app_id: res.locals.app._id});
    }).then(function (token){
      return res.status(202).json(token);
    }).catch(function (err){
      return next(err);
    })

    //return res.json(res.locals.data);
  })

  app.use("/oauth", router);
}
