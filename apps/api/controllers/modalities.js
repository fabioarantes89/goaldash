var express = require("express");
var router = express.Router();

module.exports = function (app){
  // Param
  router.param("id", function (req, res, next){
    app.locals.database.models.Modality.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  // SHOW
  router.get("/:id", function(req, res){
    return res.json(res.locals.data);
  })
  // UPDATE
  router.put("/:id", app.locals.hasToken, function(req, res){
    app.locals.database.models.Modality.findOneAndUpdate({_id: req.params.id}, req.body, {new: false, runValidators: true}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // DELETE
  router.delete("/:id", app.locals.hasToken, function (req, res){
    app.locals.database.models.Modality.remove({_id: req.params.id}).exec().then(function (){
      return res.status(200).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // CREATE
  router.post("/", app.locals.hasToken, function (req, res){
    app.locals.database.models.Modality.create(req.body).then(function (data){
      return res.status(202).json(data);
    }).catch(function (err){
        return res.json(err.errors).status(500);
    });
  })
  // LIST
  router.get("/", function(req, res){
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;
    var query = {};

    if(req.query.term){
      query.name = new RegExp(req.query.term, "i");
    }

    app.locals.database.models.Modality.count(query).exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Modality.find(query).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){
        res.locals.data = data;
        return res.json(res.locals);

      })
    })
    .catch(function(err){
      return next(err);
    })
  })
  app.use("/modalities", router);
}
