var express = require("express");
var router = express.Router();

module.exports = function (app){
  // Param
  router.param("id", function (req, res, next){
    app.locals.database.models.File.find({_id: req.params.id}).exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  router.get("/:id", function (req, res, next){
    return res.json(res.locals.data);
  })
  router.post("/", app.locals.hasToken, app.locals.uploads.single('file'), function (req, res, next){
  //router.post("/", app.locals.hasToken, app.locals.uploads.any(), function (req, res, next){
    //console.log(req);
    console.log(req.rawBody)

    app.locals.database.models.File.create({"filename": req.file.filename, type: req.file.mimetype}).then(function (file){
        return res.status(202).json(file)
    }).catch(function (err){
      return next(err)
    })
  })

  app.use("/files", router);
}
