var express = require("express");
var router = express.Router();
var PushService = require("../../../lib/pushService.js")();
var Promise = require("bluebird");

function pushServ(data){
  return new Promise(function (resolve, reject){

    var pushList = [];
    data.map(function (team){
        if(team.owner.device_id && team.owner.device_type){
          pushList.push(new PushService.PushDevice(team.owner.device_id, team.owner.device_type));
        }
        team.players.map(function (player){
          if(player.device_id && player.device_type){
            pushList.push(new PushService.PushDevice(player.device_id, player.device_type));
          }
        })
    })
    if(pushList.length > 0){
      PushService.sendMultipleMessages("YOUR_TEAM_HAS_A_NEW_MATCH", pushList);
    }
    return resolve();
  })
}

module.exports = function (app){
  // Param
  router.param("id", function (req, res, next){
    app.locals.database.models.Match.find({_id: req.params.id}).populate("team_home team_visitor file").exec().then(function (data){
      if(!data[0]) { return next(new Error("Content not found")); }
      res.locals.data = data[0];
      return next();
    }).catch(function (){
      return next(new Error("Content not found"));
    })
  })
  // SHOW
  router.get("/:id", function(req, res){
    return res.json(res.locals.data);
  })

  // UPDATE
  router.put("/:id", app.locals.hasToken, function(req, res){
    if(req.body.team_home){
      delete req.body.team_home;
    }
    if(req.body.team_visitor){
      delete req.body.team_visitor;
    }
    var match = null;
    app.locals.database.models.Match.findOneAndUpdate({_id: req.params.id}, req.body, {new: false, runValidators: true}).exec().then(function (data){
      match = data;
      return app.locals.database.models.Team.find({
        _id: {$in: [data.team_home, data.team_visitor]}
      }).populate("players").exec();
    }).then(function(data){
      return pushServ(data);
    }).then(function (data){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // DELETE
  router.delete("/:id", app.locals.hasToken, function (req, res){
    app.locals.database.models.Match.remove({_id: req.params.id}).exec().then(function (){
      return res.status(202).end();
    }).catch(function (){
      return res.status(503).end();
    })
  })
  // CREATE
  router.post("/", app.locals.hasToken, function (req, res){
    if(req.body.score_visitor){
      delete req.body.score_visitor;
    }
    if(req.body.score_home){
      delete req.body.score_home;
    }
    var match = null;
    app.locals.database.models.Match.create(req.body).then(function (data){
      match = data;
      return app.locals.database.models.Team.find({
        _id: {$in: [data.team_home, data.team_visitor]}
      }).populate("players owner").exec();

    }).then(function(data){
      //console.log(data);
      var service = pushServ(data);
      return service
    }).then(function (data){
      return res.status(202).json(match);
    }).catch(function (err){
      console.log(err);
        return res.json(err.errors).status(500);
    });
  })
  // LIST
  router.get("/",  function(req, res){
    res.locals.currentPage = (req.query.page)?Number(req.query.page):0;
    res.locals.perPage = (req.query.perPage)?Number(req.query.perPage):10;
    res.locals.offset = res.locals.currentPage * res.locals.perPage;
    var query = {};

    if(req.query.term){
      query.name = new RegExp(req.query.term, "i");
    }

    app.locals.database.models.Match.count(query).exec().then(function (count){
      res.locals.totalRows = count;
      res.locals.totalPages = Math.ceil(count/res.locals.perPage);

      return app.locals.database.models.Match.find(query).populate({path: "team_home", populate: {path: "file"}}).populate({path: "team_visitor", populate: {path: "file"}}).populate({path: "place"}).skip(res.locals.offset).limit(res.locals.perPage).exec().then(function (data){

        res.locals.data = data;
        return res.json(res.locals);

      })
    })
    .catch(function(err){
      return next(err);
    })
  })
  app.use("/matches", router);
}
