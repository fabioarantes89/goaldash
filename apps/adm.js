var express = require("express");
var session = require("express-session");
var MongoStore = require("connect-mongostore")(session);

var adm = express();


adm.set("view engine", 'jade');
adm.set("views", __dirname+"/../views/");
adm.use(session({
  secret: 'ImaginateStudioGoalDash',
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({'db': 'sessions'})
}))
module.exports = function (locals){
  adm.locals = locals;
  adm.use("/public", express.static(__dirname+"/../public/"));

  adm.get("/", function (req, res){
    res.render("index")
  })

  require(__dirname+"/adm/controllers/login.js")(adm);
  require(__dirname+"/adm/controllers/users.js")(adm);
  require(__dirname+"/adm/controllers/teams.js")(adm);
  require(__dirname+"/adm/controllers/categories.js")(adm);
  require(__dirname+"/adm/controllers/modalities.js")(adm);
  require(__dirname+"/adm/controllers/places.js")(adm);
  require(__dirname+"/adm/controllers/apps.js")(adm);

  return adm;
}
