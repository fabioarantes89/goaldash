var express = require("express");
var app = express();


module.exports = function (locals){
    app.locals = locals;

    app.locals.hasToken = function (req, res, next){
      if(!req.headers.authorization){ return next(new Error("No authorization header found"))};

      var token = req.headers.authorization.replace(/Bearer([:])? (.+)/i, "$2");

      app.locals.database.models.Token.find({"token": token}).exec(function(err, data){
        if(!data[0] || err) { return next(new Error("Invalid Token")) }
        var t = data[0];

        res.locals.user_id = t.user_id;
        res.locals.app_id = t.app_id;

        return next();

      }).catch(function (){
        return next(new Error("Invalid Token"))
      })




    }


    require(__dirname+"/api/controllers/categories.js")(app);
    require(__dirname+"/api/controllers/files.js")(app);
    require(__dirname+"/api/controllers/championships.js")(app);
    require(__dirname+"/api/controllers/login.js")(app);
    require(__dirname+"/api/controllers/matches.js")(app);
    require(__dirname+"/api/controllers/modalities.js")(app);
    require(__dirname+"/api/controllers/places.js")(app);
    require(__dirname+"/api/controllers/teams.js")(app);
    require(__dirname+"/api/controllers/users.js")(app);

    app.use(function (err, req, res, next){
        var status = 500;
        if(err.message == "Content not found") {
          status = 404;
        }
        return res.status(status).json({"status": "ERROR", message: err.message});
    })




  return app;
}
